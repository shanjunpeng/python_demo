﻿# Filename: while.py


number = 23
running = True

while running:
    guess = int(input('Enter an integer:'))

    if guess == number:
        print('right')
        running = False
    elif guess < number:
        print('higher')
    else:
        print('lower')
else:
    print('the while loop is over')
print('done')
