﻿# Filename: function_default


def say(message, times=1):
    print(message * times)


say('hello')
say('world', 5)
