import time

try:
    f = open('../inputoutput/poem.txt')
    while True:
        line = f.readline()
        if len(line) == 0:
            break
        print(line, end='')
        time.sleep(2)
except KeyboardInterrupt:
    print('cancelled')

finally:
    f.close()
    print('(closed the file)')
