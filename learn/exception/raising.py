class ShortInputExecption(Exception):
    def __init__(self, length, atleast):
        Exception.__init__(self)
        self.length = length
        self.atleast = atleast


try:
    text = input('Input something:')
    if len(text) < 3:
        raise ShortInputExecption(len(text), 3)
except EOFError:
    print('WTF')
except ShortInputExecption as ex:
    print('ShortInputExecption The input was {0} long, excepted at least {1}'.format(ex.length, ex.atleast))
