'''
在屏幕后面发生的事情就是 with 语句使用了一种协议。获得了 open 语句返回的
对象，就叫做’thefile’ 好了。
在启动代码块之前，在后台总会调用 thefile.__enter__ 函数，在代码块结束后又
会调用 thefile.__exit__ 函数
'''

with open('../inputoutput/poem.txt') as f:
    for line in f:
        print(line, end='')
