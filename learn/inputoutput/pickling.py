import pickle

game_data = 'game.data'
games = ['魔兽世界', '星际战甲', '风暴英雄', '古墓丽影']

# b 二进制模式打开
f = open(game_data, 'wb')
pickle.dump(games, f)
f.close()

del games
f = open(game_data, 'rb')
games = pickle.load(f)
print(games)
