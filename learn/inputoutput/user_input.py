import string


def reverse(text):
    return text[::-1]


def is_palindrome(text):
    text = text.lower()
    text = text.replace(' ', '')
    for char in string.punctuation:
        text = text.replace(char, '')
    return text == reverse(text)


while True:
    something = input('Enter text:')
    if something == 'quit':
        break
    elif is_palindrome(something):
        print("Yes, it is a palindrome")
    else:
        print("No,it is not a palindrome")
