poem = '''\
Programming is fun
When the work is done
if you wanna make your work also fun:
use Python!
'''
# 读模式（’r’）、写模式（’w’）或追加模式（’a’）
f = open('poem.txt', 'w')
f.write(poem)
f.close()

f = open('poem.txt')
while True:
    line = f.readline()
    if len(line) == 0:  # 当一个空的字符串被返回的时候，即表示文件末已经到达了
        break
    print(line, end='')
f.close()
