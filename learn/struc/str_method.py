name = 'hello world'
if name.startswith('he'):
    print(1)
if 'world' in name:
    print(2)
if name.find('hello') != -1:
    print(3)

mylist = ['hello', 'sjp', 'xuxu', 'best']
print('__*__'.join(mylist))
