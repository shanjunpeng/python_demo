# 元组用来将多样的对象集合到一起。
# 元组和列表十分类似，只不过元组和字符串一样是不可变的即你不能修改元组
zoo = ('python', 'elephant', 'tiger')

new_zoo = ('monkey', 'camel', zoo)
print(new_zoo)
