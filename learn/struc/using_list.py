def print_shoplist(shoplist):
    print('I have', len(shoplist), 'item to purchase')

    print('These items are:', end='  ')

    for item in shoplist:
        print(item, end=',')
    print('\n')


shoplist = ['apple', 'pear', 'banana']
print_shoplist(shoplist)

shoplist.append('rice')
print_shoplist(shoplist)

shoplist.sort()
print_shoplist(shoplist)

del shoplist[0]
print_shoplist(shoplist)
