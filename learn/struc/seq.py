shoplist = ['apple', 'pear', 'banana', 'rice', 'bag']
name = 'thinking in java'

print(shoplist[0])
print(shoplist[-1])
# 前闭后开
print(shoplist[0: -1])

# 第三个参数，切片的步长
print(shoplist[::2])
print(shoplist[::-1])

print(name[1:3])
print(name[0:-1])
