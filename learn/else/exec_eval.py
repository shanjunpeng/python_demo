# exec 语句用来执行储存在字符串或文件中的 Python 语句
exec('print("hello")')

# eval 函数用来执行存储在字符串中的 Python 表达式
r = eval('2 * 3')
print(r)