# 在一个函数中返回两个不同的值吗？你可以。要做的就是使用元组
def get_error_details():
    return (2, 'details')


errornum, errordetails = get_error_details()
print(errornum)
print(errordetails)

a, *b = [1, 2, 3, 4]
print(a)
print(b)

# 交换两个变量
a = 5
b = 8
a, b = b, a
print(a, b)
