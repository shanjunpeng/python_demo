# 通过列表综合，可以从一个已有的列表导出一个新的列表
list1 = [1, 3, 4]
list2 = [2 * i for i in list1 if i > 2]

print(list2)
