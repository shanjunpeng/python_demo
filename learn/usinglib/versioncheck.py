import sys
import warnings

print(sys.version_info)
print(sys.version)

if sys.version_info[0] < 3:
    warnings.warn('Need Python3 for this program to run ', RuntimeWarning)
else:
    print('OK!')
