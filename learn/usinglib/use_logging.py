import logging, os, platform

if platform.platform().startswith('Windows'):
    # logging_file = os.path.join(os.getenv('HOMEDRIVE'), 'text.log')
    logging_file = 'D:\\text.log'
else:
    logging_file = os.path.join(os.getenv('HOME'), 'text.log')

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s : %(levelname)s : %(message)s',
                    filename=logging_file,
                    filemode='w')

logging.debug('start the program')
logging.info('doing something')
logging.warning('Dying now')
