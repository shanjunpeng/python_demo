import sys
import json
import urllib, urllib.parse, urllib.request, urllib.response

YAHOO_APP_ID = 'dj0yJmk9Z0YxQ1h4aUtPSUh5JmQ9WVdrOWJWQkJSR1phTlRZbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD05ZQ--'
SEARCH_BASE = 'http://search.yahooapis.com/WebSearchService/V1/webSearch'


class YahooSearchError(Exception):
    pass


def search(query, results=20, start=1, **kwargs):
    kwargs.update({
        'appid': YAHOO_APP_ID,
        'query': query,
        'results': results,
        'start': start,
        'output': 'json'

    })

    url = SEARCH_BASE + '?' + urllib.parse.urlencode(kwargs)
    result = json.load(urllib.request.urlopen(url))
    if 'Error' in result:
        raise YahooSearchError(result['Error'])
    return result['ResultSet']


query = input('search:')
for result in search(query)['Result']:
    print("{0}:{1}".format(result['Title'], result['Url']))
