﻿# Filename: for.py

# range不包括第二个数。1,5实际为1到4
for i in range(1, 5):
    print(i)
else:
    print('the for loop is over')

# 第三个参数为步长
for i in range(1, 5, 2):
    print(i)
