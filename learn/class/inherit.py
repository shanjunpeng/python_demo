class SchoolMember:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        print('(init SchoolMember {0})'.format(self.name))

    def tell(self):
        print('Name:{0} Age:{1}'.format(self.name, self.age), end=' ')


class Teacher(SchoolMember):
    def __init__(self, name, age, salary):
        SchoolMember.__init__(self, name, age)
        self.salary = salary
        print('(init Teacher {0})'.format(self.name))

    def tell(self):
        SchoolMember.tell(self)
        print('Salary:{0}'.format(self.salary))


class Student(SchoolMember):
    def __init__(self, name, age, marks):
        SchoolMember.__init__(self, name, age)
        self.marks = marks
        print('(init Student {0})'.format(self.name))

    def tell(self):
        SchoolMember.tell(self)
        print('Marks:{0}'.format(self.marks))


t = Teacher('Mr Black', 30, 30000)
s = Student('Jack', 12, 75)

print()

members = [t, s]
for m in members:
    m.tell()
