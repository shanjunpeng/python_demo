class Robot:
    propulation = 0

    def __init__(self, name):
        self.name = name
        print('(init {0})'.format(self.name))

        Robot.propulation += 1

    def __del__(self):
        print('{0} is being destroyed'.format(self.name))
        Robot.propulation -= 1

    def sayhi(self):
        '''Greeting by the robot'''
        print('hi,my name is {0}'.format(self.name))

    @staticmethod
    def how_many():
        print('We have {0:d} robots'.format(Robot.propulation))


r1 = Robot('r001')
r1.sayhi()
Robot.how_many()

r2 = Robot('c3-p0')
r2.sayhi()

Robot.how_many()

del r1
del r2

Robot.how_many()


print(Robot.sayhi.__doc__)