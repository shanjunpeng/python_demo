# 当我们定义一个带星的参数，像 *param 时，从那一点后所有的参数被收集为一个叫做 ’param’ 的列表
# 在带星号的参数后面申明参数会导致 keyword-only 参数


def total(init=5, *numbers, **keywords):
    count = init
    for number in numbers:
        count += number
    for key in keywords:
        count += keywords[key]
    return count


count = total(10, 1, 2, 3, apple=50, pear=100)
print(count)
