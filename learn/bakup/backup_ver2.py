import os
import time

source = ['"C:\\Users\\vcc\\Desktop\\学习\\mysql"']
target_dir = 'E:\\Backup'

today = target_dir + os.sep + time.strftime('%Y%m%d')
now = time.strftime('%H%M%S')

if not os.path.exists(today):
    os.mkdir(today)
    print('成功创建目录', today)

target = today + os.sep + now + '.zip'

zip_command = 'zip -qr {0} {1}'.format(target, ' '.join(source))
print(zip_command)

if os.system('zip') == 0:
    print('Successful backup to', target)
else:
    print('Backup FAILED')
