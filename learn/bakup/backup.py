import os
import time
import zipfile

source = ['C:\\Users\\vcc\\Desktop\\学习\\mysql']
target_dir = 'E:\\Backup'

today = target_dir + os.sep + time.strftime('%Y%m%d')
now = time.strftime('%H%M%S')

if not os.path.exists(today):
    os.mkdir(today)
    print('成功创建目录', today)

target = today + os.sep + now + '.zip'
print(target)

azip = zipfile.ZipFile(target, 'w')
for src in source:
    azip.write(src, compress_type=zipfile.ZIP_LZMA)
azip.close()

print('Successful backup to', target)
