# https://leetcode-cn.com/problems/number-of-boomerangs/description/

def distanceOf(p1, p2):
    return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])


class Solution:
    def numberOfBoomerangs(self, points):
        """
        :type points: List[List[int]]
        :rtype: int
        """

        for i in range(0, len(points)):
            dic = {}
            for j in range(i + 1, len(points)):
                distance = distanceOf(points[i], points[j])
