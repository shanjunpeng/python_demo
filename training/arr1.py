class Solution:

    def remove_duplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        # print(nums)
        # print('len={0}'.format(len(nums)))

        for i in range(0, len(nums)):
            # print(nums[i - 1], nums[i])
            if i < len(nums) - 1 and nums[i] is not None:
                while True:
                    if nums[i] == nums[i + 1] and nums[i] is not None and nums[i + 1] is not None:
                        for j in range(i, len(nums) - 1):
                            # print(nums[j], nums[j + 1])
                            nums[j] = nums[j + 1]
                            nums[j + 1] = None
                    else:
                        break

        # print(nums)

        count = 0
        for n in nums:
            if n is not None:
                count = count + 1
        return count


s = Solution()
c = s.remove_duplicates(
    [1, 2, 3, 3, 3]
)
print('count={0}'.format(c))
