class Solution:
    def mergeTrees(self, t1, t2):
        """
        :type t1: TreeNode
        :type t2: TreeNode
        :rtype: TreeNode
        """
        max_len = len(t1) if len(t1) > len(t2) else len(t2)
        ret = [None for i in range(0, max_len)]
        print(ret)

        for i in range(0, max_len):
            a = t1[i] if len(t1) > i else None
            b = t2[i] if len(t2) > i else None
            if a is not None and b is not None:
                ret[i] = a + b
            elif a is not None:
                ret[i] = a
            elif b is not None:
                ret[i] = b

        return ret


s = Solution()
result = s.mergeTrees([1, 3, 2, 5], [2, 1, 3, None, 4, None, 7])
print(result)
