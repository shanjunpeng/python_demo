class Solution:
    def numJewelsInStones(self, J, S):
        """
        :type J: str
        :type S: str
        :rtype: int
        """
        count = 0
        for i in range(0, len(S)):
            try:
                J.index(S[i])
                count = count + 1
            except ValueError:
                print('no')

        return count


s = Solution()
result = s.numJewelsInStones('aA', 'aAAbbbb')
print(result)
