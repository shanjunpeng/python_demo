class Solution:
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        nums_dict = {}
        for i in range(0, len(nums)):
            n = target - nums[i]
            if nums_dict.__contains__(n):
                return [nums_dict.get(n), i]
            else:
                nums_dict[nums[i]] = i


s = Solution()
result = s.twoSum(
    [2, 7, 11, 15],
    9)
print(result)
