# https://leetcode-cn.com/problems/reverse-string/description/


class Solution:
    def reverseString(self, s):
        """
        :type s: str
        :rtype: str
        """
        return s[::-1]


sol = Solution()
result = sol.reverseString("112fsffs")
print(result)
