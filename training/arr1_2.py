class Solution:

    def remove_duplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        # print(nums)
        # print('len={0}'.format(len(nums)))

        for n in nums:
            while nums.count(n) > 1:
                del nums[nums.index(n)]

        # print(nums)
        return len(nums)


s = Solution()
c = s.remove_duplicates(
    [1, 2, 3, 3, 3]
)
print('count={0}'.format(c))
